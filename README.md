# Web Crawler Application

Web Crawler Application is a Nodejs and Angular application for websites scarping.

## Solution Overview
The user enters the ```startUrl```, ```maxDepth``` and ```maxPages```. That info is sent to the server which handles all the logic. 
The server runs on the first url that was given, using the BFS algorithm it runs over all the HTML and looks for ```<a href...>``` tags in the current depth and saves them. After if finished running over all the urls of start url and saved them it takes the next url from the saved urls and does the same thing.
Every iteration it checks that the current depth and total pages not exceeded.
After the proccess finished it returns all the data to the client.
The BFS here is implemented in the way we go into depth, first all the urls in the current depth are checked and only after it's finished we go to the next depth.


## Dependencies Installation

First install NodeJs, Use the node package manager NPM to install all required packages.

```bash
npm install
```
## Run The Backend
Use NodeJs to run the backend.
```bash
cd Backend
npm start
```
Frontend is served in ```http://localhost:3000```

## Run The Frontend
Use Angular CLI to run the frontend.
```bash
cd Frontend
npm install -g @angular/cli
ng serve --prod
```
Frontend is served in ```http://localhost:4200```

## Run The Backend Tests
Use NodeJs to run the tests.
```bash
cd Backend
npm run test
```